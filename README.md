# meetup-initflow

# Gitlab CI/CD

## Build

## Deploy

docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY

gitlab-ci-token - создается автоматически
$CI_BUILD_TOKEN - переменная доступная внутри gitlab-ci


# Build on dockerhub with github.com as code source

1. Create account on [hub.docker.com](https://hub.docker.com/)

2. Create repository for project and link with github repository

![img.png](img/hub_docker_create_repo.png)

3. Configure builds: Configure automated builds
![img.png](img/hub_docker_build_config.png)
   
4. Trigger build

![img.png](img/hub_docker_build.png)

5. On successful build, check Tags

![img.png](img/hub_docker_tags.png)

6. Use image 
- arck1/meetup-initflow:latest
- registry.hub.docker.com/arck1/meetup-initflow:latest
