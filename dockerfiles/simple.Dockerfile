FROM golang:1.16 AS dependencies
ENV NAME "web"
WORKDIR /opt/${NAME}

COPY .. .
RUN ["make", "build"]

CMD ./bin/${NAME}