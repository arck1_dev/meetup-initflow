package main

import (
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
)

func Index(ctx *gin.Context) {
	ctx.HTML(http.StatusOK, "index.html", nil)
}

func GetAnswer(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{"answer": GetRandomAnswer()})
}

func main() {
	router := gin.Default()
	router.GET("/api/answer/", GetAnswer)
	router.StaticFile("/", "./static/index.html")
	router.StaticFile("/favicon.ico", "./static/favicon.ico")
	router.Static("/static/", "./static/")
	baseEndPoint := ":8080"
	maxHeaderBytes := 1 << 20

	server := &http.Server{
		Addr:           baseEndPoint,
		Handler:        router,
		MaxHeaderBytes: maxHeaderBytes,
	}

	log.Printf("[info] start http server listening %s", baseEndPoint)
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
