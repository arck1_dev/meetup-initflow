var app = new Vue({
    el: '#app',
    data: {
        message: ""
    },
    methods: {
        GetAnswer: function (event) {
            fetch("/api/answer/")
                .then(response => response.json())
                .then(data => {
                    this.message = data.answer
                })
        }
    },
    beforeMount(){
        this.GetAnswer()
    },
})
